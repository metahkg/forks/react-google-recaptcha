import React from "react";
import ReCAPTCHA from "./recaptcha";
import makeAsyncScriptLoader from "react-async-script";
import PropTypes from "prop-types";

const callbackName = "onloadcallback";
const globalName = "grecaptcha";

function getOptions() {
  return (typeof window !== "undefined" && window.recaptchaOptions) || {};
}

function getURL(options = { useTurnstile: false }) {
  const dynamicOptions = getOptions();
  if (options.useTurnstile) {
    return "https://challenges.cloudflare.com/turnstile/v0/api.js?compat=recaptcha";
  }
  const hostname = dynamicOptions.useRecaptchaNet ? "recaptcha.net" : "www.google.com";
  return `https://${hostname}/recaptcha/api.js?onload=${callbackName}&render=explicit`;
}

const ReCAPTCHAWrapper = React.forwardRef(function ReCAPTCHAWrapper(props, ref) {
  const Component = makeAsyncScriptLoader(getURL({ useTurnstile: props.useTurnstile }), {
    callbackName,
    globalName,
    attributes: { ...(getOptions().nonce && { nonce: getOptions().nonce }) },
  })(ReCAPTCHA);
  return <Component {...props} ref={ref} />;
});

ReCAPTCHAWrapper.propTypes = {
  useTurnstile: PropTypes.bool,
};

export default ReCAPTCHAWrapper;
